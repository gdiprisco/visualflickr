# VisualFlickr
## Requirements

- [Caffe](https://caffe.berkeleyvision.org/) deep learning framework installed on your machine  
  If you use *__aptitude__* package manager, you can run:
  ```
  apt install caffe-cpu
  ```
  or
  ```
  apt install caffe-cuda
  ```    

- Python 3.x
- You can install all the necessary packages running:
  ```
  pip install -r requirements.txt
  ``` 
 - ImageTK  
   If you use *__aptitude__* package manager, you can run
     ```
     apt install python3-pil.imagetk
     ```
## Tested
VisualFlickr has been tested on __Ubuntu 18.04__ in Tilix terminal emulator and in PyCharm IDE.

## Run
Run __demo.py__ in your Python interpreter.

